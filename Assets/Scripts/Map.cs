﻿using UnityEngine;
using System.Collections;

public class Map : MonoBehaviour {

    public Tile tilePrefab;

    public uint width = 0;
    public uint height = 0;

	public uint bombs = 0;

	Canvas gridCanvas;
	SquareMesh squareMesh;

    public Tile[,] tiles; 

    void Awake ()
	{
		tiles = new Tile[width, height];
		for (int z = 0; z < height; z++)
		{
			for (int x = 0; x < width; x++)
			{
				CreateTile(x, z);
			}
		}

		gridCanvas = GetComponentInChildren<Canvas>();
		squareMesh = GetComponentInChildren<SquareMesh>();
	}

	void CreateTile(int x, int y)
	{
		Tile cell = tiles[x, y] = Instantiate<Tile>(tilePrefab);
		cell.transform.SetParent(transform, false);
		cell.coords.x = x;
		cell.coords.y = y;
	}

	void Start()
	{
		squareMesh.Triangulate(tiles);
	}
}
