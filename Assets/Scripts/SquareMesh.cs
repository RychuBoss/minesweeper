﻿using UnityEngine;
using System.Collections.Generic;



[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class SquareMesh : MonoBehaviour
{
	Mesh mesh;
	List<Vector3> vertices;
	List<int> triangles;

	public float tile_size = 2.0f;
	public const float fill_size = 0.9f;
	const float margin = (1 - fill_size) / 2;

	void Awake()
	{
		GetComponent<MeshFilter>().mesh = mesh = new Mesh();
		mesh.name = "Squere Mesh";
		vertices = new List<Vector3>();
		triangles = new List<int>();
	}

	public void Triangulate(Tile[,] tiles)
	{
		clear_mesh();
		TrianulateAllTiles(tiles);
		init_mesh();
	}

	private void init_mesh()
	{
		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.RecalculateNormals();
	}

	private void clear_mesh()
	{
		mesh.Clear();
		vertices.Clear();
		triangles.Clear();
	}

	private void TrianulateAllTiles(Tile[,] tiles)
	{
		for (int y = 0; y < tiles.GetLength(0); y++)
		{
			for (int x = 0; x < tiles.GetLength(0); x++)
			{
				Triangulate(tiles[x, y]);
			}
		}
	}

	void Triangulate(Tile tile)
	{
		float side = tile_size * fill_size;

		Vector3 top_left = calculate_top_left(tile);
		Vector3 top_right = top_left + side * Vector3.right;
		Vector3 bot_left = top_left + side * Vector3.down;
		Vector3 bot_right = top_left + side * (Vector3.right + Vector3.down);

		tile.transform.localPosition = bot_left;
		AddTriangle(top_left, top_right, bot_left);
		AddTriangle(bot_left, top_right, bot_right);
	}

	private Vector3 calculate_top_left(Tile tile)
	{
		Vector3 top_left = Vector3.zero;
		top_left += (tile.coords.x + margin) * tile_size * Vector3.right;
		top_left += (tile.coords.y + margin) * tile_size * Vector3.up;
		return top_left;
	}

	void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
	{
		int vertexIndex = vertices.Count;
		vertices.Add(v1);
		vertices.Add(v2);
		vertices.Add(v3);
		triangles.Add(vertexIndex);
		triangles.Add(vertexIndex + 1);
		triangles.Add(vertexIndex + 2);
	}
}